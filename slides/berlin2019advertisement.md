% Berlin 2019

## Berlin, One of the World's Great Cities

![](brandenburg-gate-417890_1920.jpg)

- Capital of Germany
- Rich in history and culture
- *Everyone* speaks English!

## Course Offerings

- CS 2340 Objects and Design (Prereqs: CS 1331)
  - Required for all CS majors
- CS 4641 Machine Learning (Prereqs: CS 1331 or CS 2316)
  - Very difficult to get in on ATL campus
  - Satisfies "pick" requirements for Intelligence and Modeling & Simulation threads, elective for Theory CS thread
  - Satisfies elective for Analytics concentration in ISyE
- CS 4001 Computing and Society
  - Required for all CS majors (can satisfy ethics requirement for other majors?)
- CS 2701/COE 2701/MGT 4803 Startup Lab
  - Satisfies portion of junior design requirement for CS majors
- ISYE 3770 Intro Statistics and Applications (Prereqs: Some combo of multivariable calc (Calc III) and basic linear algebra)
- APPH 1040/APPH 3801 Scientific Foundations of Health
  - Required for all GT undergraduates

## Faculty


\begin{tabular}{cc}\\
\includegraphics[height=1in]{owen_1.jpg}         & \includegraphics[height=1in]{jean-sands.jpg} \\
Henry Owen, ECE                            & Jean Sands, ECE                        \\
CS/COE 2701, MGT 4803, CS 4001                   & ISyE 3770                                    \\
\includegraphics[height=1in]{chris-simpkins-headshot.jpg} & \includegraphics[height=1in]{caroline-simpkins-headshot.jpg}\\
Christopher Simpkins, CoC                  & Caroline Simpkins, APPH \\
CS 2340, CS 4641                                 & APPH 1040, APPH 3801 \\
\end{tabular}

## Information and Signing Up

Visit this site for details as they become available:

- [[http://www.cc.gatech.edu/berlin]]

Sign-up is through OIE's Atlas system:

- [[https://atlas.gatech.edu/]]
- Under Programs search for "Berlin Summer Program"
- Taking the first 30 eligible students who sign up and pay non-refundable deposit
- Selections will take place in October 2018

Questions:

- Dawn Rutherford [[mailto:drutherf@cc.gatech.edu][drutherf@cc.gatech.edu]]
