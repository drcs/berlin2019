---
layout: default
name: Berlin 2019 Courses
---

# Berlin 2019 Courses

To paraphrase Goethe, those who don't know foreign countries don't know their own. Through organized local cultural excursions and by living in a German dormitory and commuting a short distance to classes at the Technical University of Berlin, students will gain first-hand experience of the rich culture and diversity of life in a major foreign city. In their classes, material will be tailored to the Berlin location to further explore the commonalities and differences between life, learning, and commerce in Berlin versus the United States.

## Course Descriptions

In CS4001 Computing, Society, & Professionalism students will investigate the societal impact of the Berlin high tech scene and startup environment. The Berlin environment will be used to explore the ethical and social challenges in international high tech in general and international startups in particular. CS4001 course materials will be enhanced to include Berlin and European Union specific course materials in a manner similar to how CS4001 has previously been enhanced in the Oxford program. Beki Grinter, who has successfully enhanced CS4001 to include European Union issues, is advising us in our tailoring of CS 4001 to the Berlin program.

In CS 2701 Startup Lab students will be introduced to international entrepreneurship focusing specifically on startup culture and infrastructure in Berlin. Berlin has long been considered the startup capital of Europe (a title it sometimes trades with other major European cites year by year).  As originally suggested by Steve McLaughlin, offering the Startup Lab course in international programs will be an additional motivation for Georgia Tech students to interact with the local communities, thus enhancing the international experience. The Startup lab course in Berlin will expose Georgia Tech students to the Berlin startup culture and support infrastructure. In addition, the college of computing has added CS2701 Startup Lab to its design sequence,  CS students participating in the Berlin Summer program may count CS2701 toward their required design sequence.

In CS 4641 Machine Learning students learn how to create computer programs that learn from data.  The faculty of the Berlin program have many years of experience in integrating international themes into data-oriented Computer Science courses, for example, creating database models of the French rail system or analyzing statistical data gathered by the European Commission in order to gain insight into European society and commerce.  Machine Learning is the hottest growth area in hight tech at the moment, with large companies scrambling to build machine learning/data science programs and startups being born every day. With its rapid growth and reach into personal lives, machine learning has also been the topic of ethical debates concerning privacy, bias, and social justice. With its clear connections to ethics and startups, CS 4641 will be designed to complement and enhance the other course being offered in Berlin.

In CS 2340 Objects and Design students learn how to design and build software systems in teams.  In the Berlin program the course will be designed to complement and enhance the other courses being offered in Berlin. High tech startups produce significant software systems in small teams, many modern startups use machine learning in their products, and software systems designed for human users must consider ethical issues such as data protection, privacy and cultural sensitivity.

In APPH 1040 students learn the fundamentals of physical and mental well-being -- a topic of critical importance to students in a stressful elite technical university. Studying abroad presents additional challenges in maintaining physical health and managing stress. The faculty of the Berlin program will tailor APPH 1040 to address these challenges, equipping students with the ability to adapt their eating habits and personal fitness routines to international environments in order to maintain their mental and physical well-being.

Finally, just as the faculty of the Berlin program have done with their Atlanta campus courses, Computer Science courses offered in Berlin will feature Serve-Learn-Sustain themes, which will be tailored to the Berlin environment.  Students will gain an appreciation for global and local issues facing modern society and the roles technology can play in improving the human condition around the world.


## Berlin Courses and CS Threeads

The course offerings in the Berlin Summer Program are designed to be maximally engaging and useful. All of these courses fulfill important requirements for your Georgia Tech degree.

- CS 2340 Objects and Design (Prereqs: CS 1331)
  - Required for all CS majors
- CS 4641 Machine Learning (Prereqs: CS 1331 or CS 2316)
  - Very difficult to get in on ATL campus
  - Satisfies "pick" requirements for Intelligence and Modeling & Simulation threads, elective for Theory CS thread
  - Satisfies elective for Analytics concentration in ISyE
- CS 4001 Computing and Society
  - Required for all CS majors (can satisfy ethics requirement for other majors?)
- CS 2701/COE 2701/MGT 4803 Startup Lab
  - Satisfies portion of junior design requirement for CS majors
- ISYE 3770 Intro Statistics and Applications (Prereqs: Some combo of multivariable calc (Calc III) and basic linear algebra)
- APPH 1040/APPH 3801 Health
  - Required for all GT undergraduates
