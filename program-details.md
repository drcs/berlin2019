---
layout: default
name: Berlin Summer Program Details
---

# Berlin Summer Program Details

The Berlin Summer Program ...

Join us for an information session with program faculty and alumni to learn more!   Information sessions are generally scheduled throughout the fall semester.   For upcoming dates and other details, email  <a href="mailto:berlin@cc.gatech.edu" style="box-sizing: border-box; color: rgb(26, 13, 171); text-decoration: none;">berlin@cc.gatech.edu</a>!</p>

## Courses

The  summer courses offered through the Berlin program are open to all majors and are usually taught by 3 to 4 GT faculty, in English, for GT resident credit. Courses are typically offered in math, isye, computer science and applied physiology, and classes are held at the Technical University of Berlin.  All courses in Berlin are typically scheduled to avoid any overlap or conflict in students’ selection, and students are expected to register for 12 credit hours for the summer.  The courses often draw on interdisciplinary themes and exploit the unique learning setting of Berlin to optimize students’ academic experience abroad.</p>

[Discover Course Offerings](berlin-courses.html)

## Costs

Students spending their summer studying in Berlin can expect to pay an application and program fee, in-state tuition (plus $250 for out-of-state students) and optional group airfare.  These costs are charged and paid through the Bursar’s Office via student accounts.  The program fee generally includes housing, health insurance and transportation to and from the airport in Berlin.  In addition, students should budget additional money for daily meals and transportation as well as leisure activities and travel.  Most Georgia Tech financial aid and scholarships are applicable to studying abroad in Berlin, including HOPE, and additional scholarships are available.

[Berlin Summer Program Costs](berlin-costs.html)

Please note our cancellation policy: the application fee is non-refundable; students cancelling participation prior to March 11th must pay the first program fee payment of $3,300; cancellations after this date will result in forfeiture of all anticipated or paid program fees. Additionally, Georgia Tech reserves the right to alter or cancel this program due to low enrollment, unavailability of a professor to teach a planned course, or other unforeseen circumstances. If Georgia Tech cancels the program before departure for reasons within its control, all fees paid by participants will be refunded. If Georgia Tech cancels the program before departure or while the program is in progress for reasons beyond its control, such as political unrest or danger to participants’ safety, only those fees that Georgia Tech is able to have refunded by service providers will be returned to participants.</p>

## Housing

## Next Steps

After receiving notice of acceptance to the program, there are important decisions to make, dates to know and meetings to attend.  First and foremost, all students should have a passport or apply for one!

[Next Steps to Berlin Summer Program](berlin2019-next-steps.html)