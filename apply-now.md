---
layout: default
name: About Berlin
---

The program accepts approximately 60 students for participation each summer – and applications will be reviewed in the order they are received – so we recommend applying early to ensure a spot!  Applications for each summer program are accepted during the preceding August through December or until the program is filled. To learn more about the application process, click [here](berlin2019-apply-now.pdf).